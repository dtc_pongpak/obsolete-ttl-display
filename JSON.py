import json
import os
import time

DEBUG_MODE = False

def read(file_name, fall_back_file_name = '', absolute_path = False):
    '''
    read(file_name, fall_back_file_name = '')

    Purpose
        Read data from a JSON file

    Parameters
        file_name: file name
        fall_back_file_name: fall-back file name
        absolute_path: file name is absolute path (True or False)
    '''
    json_dict = {}
    json_string = ''
    attempt = 0

    if absolute_path:
        full_path = file_name
    else:
        full_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), file_name)

    try:
        if DEBUG_MODE:
            print('Reading ' + file_name + '...')

        while json_string == '' and attempt < 10: # Prevent reading a blank file while the file is being written
            attempt = attempt + 1

            with open(full_path) as file:
                json_string = file.read()

            if json_string == '':
                time.sleep(0.1) # Wait for content
    except:
        if DEBUG_MODE:
            print('File not found')

    if json_string != '':
        json_dict = json.loads(json_string)
    else:
        if fall_back_file_name != '':
            if absolute_path:
                full_path = fall_back_file_name
            else:
                full_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), fall_back_file_name)

            if DEBUG_MODE:
                print('Reading ' + fall_back_file_name + '...')

            try:
                with open(full_path) as file:
                    json_string = file.read()
            except:
                if DEBUG_MODE:
                    print('File not found')

            if json_string != '':
                json_dict = json.loads(json_string)

    if DEBUG_MODE:
        print(json_dict)

    return json_dict

def write(json_dict, file_name, fall_back_file_name = '', absolute_path = False):
    '''
    write(file_name, json_dict)

    Purpose
        Write data to a JSON file

    Parameters
        json_dict: JSON-format data
        file_name: file name
        fall_back_file_name: fall-back file name
        absolute_path: file name is absolute path (True or False)
    '''
    if absolute_path:
        full_path = file_name
    else:
        full_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), file_name)

    if DEBUG_MODE:
        print('Writing to ' + file_name + '...')
        print(json_dict)

    with open(full_path, 'w') as file:
        json.dump(json_dict, file)
        file.flush()
        os.fsync(file)

    if fall_back_file_name != '':
        if absolute_path:
            full_path = fall_back_file_name
        else:
            full_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), fall_back_file_name)

        if DEBUG_MODE:
            print('Writing to ' + fall_back_file_name + '...')
            print(json_dict)

        with open(full_path, 'w') as file:
            json.dump(json_dict, file)
            file.flush()
            os.fsync(file)
