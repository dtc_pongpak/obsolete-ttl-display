from PIL import Image, ImageFont, ImageDraw
import JSON
import logging
import os
import rgbmatrix
import serial
import subprocess
import threading
import time

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
LOG_DIR = os.path.join(ROOT_DIR, 'log')

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename=os.path.join(LOG_DIR, 'panel.log'))

DEBUG_MODE = False
JSON.DEBUG_MODE = DEBUG_MODE

# =================
# = Configuration =
# =================
CONFIG_FILE = 'config.json'
config = JSON.read(CONFIG_FILE)

# =======================
# = RGB matrix constant =
# =======================
MATRIX_WIDTH = config['panel']['matrix_width']
MATRIX_HEIGHT = config['panel']['matrix_height']
CASCADE = config['panel']['cascade']
PARALLEL = config['panel']['parallel']
PANEL_WIDTH = MATRIX_WIDTH * CASCADE
PANEL_HEIGHT = MATRIX_HEIGHT * PARALLEL

# ===============
# = Serial port =
# ===============
CODEC = 'ascii'
ser = serial.Serial(port = '/dev/serial0',
                    baudrate = 9600,
                    parity = serial.PARITY_NONE,
                    stopbits = serial.STOPBITS_ONE,
                    bytesize = serial.EIGHTBITS,
                    timeout = None)
ser.close()
ser.open()

# ====================
# = Display constant =
# ====================
FONT_SCALE = config['display']['font_scale']

# ==============
# = RGB matrix =
# ==============
options = rgbmatrix.RGBMatrixOptions()
options.rows = MATRIX_HEIGHT
options.cols = MATRIX_WIDTH
options.chain_length = CASCADE
options.parallel = PARALLEL
options.pwm_bits = 3
options.pwm_lsb_nanoseconds = 300 # Increase if a panel is flickering

with open('/proc/device-tree/model') as f:
    version = f.read()

if 'Zero' in version:
    options.gpio_slowdown = 1
else:
    options.gpio_slowdown = 2

# ====================
# = Global variables =
# ====================
run_thread = True
new_temp = -25
old_temp = 999

def serial_write(data):
    if DEBUG_MODE:
        print('Serial write: ' + data)

    ser.write(data.encode(CODEC))
    ser.flush() # Wait until all data is written

def serial_read():
    global DEBUG_MODE

    if DEBUG_MODE:
        print('Reading...')

    serial_data = ser.readline()

    data = serial_data.decode(CODEC).splitlines()[0]

    if DEBUG_MODE:
        print('Serial receive: ' + data)

    return data

def read_temperature():
    global new_temp
    global run_thread

    while run_thread:
        try:
            new_temp = round(float(serial_read()))
        except:
            pass

def select_font(text, bold, italic):
    font_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'fonts')

    if bold and italic:
        font_path = os.path.join(font_dir, 'FreeSansBoldOblique.ttf')
    elif bold:
        font_path = os.path.join(font_dir, 'FreeSansBold.ttf')
    elif italic:
        font_path = os.path.join(font_dir, 'FreeSansOblique.ttf')
    else:
        font_path = os.path.join(font_dir, 'FreeSans.ttf')

    return font_path

def display_adjustment(message, bold, italic):
    font_path = select_font(message, bold, italic)

    for font_size in range(int(PANEL_HEIGHT * FONT_SCALE), 0, -1):
        font = ImageFont.truetype(font_path, font_size)
        text_width = font.getsize(message)[0]

        if text_width <= PANEL_WIDTH:
            text_height = int(font_size / FONT_SCALE)
            break

    return font, text_width, text_height

if __name__ == '__main__':
    # ==========================
    # = Start temperature read =
    # ==========================
    read_temp_thread = threading.Thread(target=read_temperature)
    read_temp_thread.start()

    # ========================
    # = Create matrix object =
    # ========================
    matrix = rgbmatrix.RGBMatrix(options = options)

    while True:
        try:
            cur_temp = new_temp

            # ===================
            # = Refresh a panel =
            # ===================
            if cur_temp != old_temp:
                # =================
                # = Draw an image =
                # =================
                # Create a blank image
                img = Image.new('RGB', (PANEL_WIDTH, PANEL_HEIGHT), (0, 0, 0))
                draw = ImageDraw.Draw(img)

                # Message and color
                message = str(cur_temp)+'°C'

                if cur_temp < 60:
                    color = (0, 255, 0)
                else:
                    color = (255, 0, 0)

                # Font and size selection
                font, text_width, text_height = display_adjustment(message, True, False)

                # Calculate spacing
                space_width = (PANEL_WIDTH - text_width) / 2
                space_height = (PANEL_HEIGHT - text_height) / 2

                # Draw image
                draw.text((space_width, space_height), message, color, font = font)

                # Display image
                matrix.SetImage(img)

                old_temp = cur_temp
        except KeyboardInterrupt:
            run_thread = False
            ser.close()
            print('Keyboard Interrupt')
            break
        except Exception as err:
            if DEBUG_MODE:
                run_thread = False
                ser.close()
                traceback.print_exc()
                break
            else:
                # Log error
                logging.error(err, exc_info=True)
